package com.zement.amazingaed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ArrayList<User> users;
    private UserAdapter adapter;

    public MainActivity() {
        users = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new UserAdapter(this, users);
        ListView list = findViewById(R.id.userList);
        list.setAdapter(adapter);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        reloadUsers();

        findViewById(R.id.addBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, AddActivity.class), 0);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("USERID", "id: " + i);
                Intent intent = new Intent(MainActivity.this, UserDetailsActivity.class);
                intent.putExtra("userUID", users.get(i).getUid());
                intent.putExtra("userName", users.get(i).getName());
                intent.putExtra("userEmail", users.get(i).getEmail());
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && data != null) {
            Map<String, Object> user = new HashMap<>();
            user.put("name", data.getStringExtra("userName"));
            user.put("email", data.getStringExtra("userEmail"));
            FirebaseFirestore.getInstance().collection("users")
                    .add(user)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    users.add(new User(task.getResult().getId(), task.getResult().get("name").toString(), task.getResult().getData().get("email").toString()));
                                    sortUsers();
                                }
                            });
                        }
                    });
        } else if(resultCode == 1001) {
            reloadUsers();
        } else if(resultCode == 1002) {
            updateUser(new User(data.getStringExtra("userUID"), data.getStringExtra("userName"), data.getStringExtra("userEmail")));
        }
    }

    private void updateUser(User user) {
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("name", user.getName());
        userMap.put("email", user.getEmail());
        FirebaseFirestore.getInstance()
                .collection("users")
                .document(user.getUid())
                .set(userMap)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e("UPDATED!", "User updated!");
                        reloadUsers();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERRORRRR", "Error getting documents.");
                    }
                });
    }

    private void reloadUsers() {
        users.clear();
        FirebaseFirestore.getInstance().collection("users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        users.add(new User(document.getId(), document.getData().get("name").toString(), document.getData().get("email").toString()));
                    }
                    sortUsers();
                } else {
                    Log.w(null, "Error getting documents.", task.getException());
                }
            }
        });
    }

    private void sortUsers() {
        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {
                return user.compareTo(t1);
            }
        });
        adapter.notifyDataSetChanged();
    }
}
