package com.zement.amazingaed;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class UserDetailsActivity extends AppCompatActivity {

    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        currentUser = new User(
                extras.getString("userUID"),
                extras.getString("userName"),
                extras.getString("userEmail"));

        ((TextInputLayout)findViewById(R.id.nameInputField)).getEditText().setText(currentUser.getName());
        ((TextInputLayout)findViewById(R.id.emailInputField)).getEditText().setText(currentUser.getEmail());

        findViewById(R.id.deleteBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUser();
            }
        });

        findViewById(R.id.saveBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getIntent().getExtras().clear();
                getIntent().putExtra("userUID", currentUser.getUid());
                getIntent().putExtra("userName", ((TextInputLayout)findViewById(R.id.nameInputField)).getEditText().getText().toString());
                getIntent().putExtra("userEmail", ((TextInputLayout)findViewById(R.id.emailInputField)).getEditText().getText().toString());
                setResult(1002, getIntent());
                finish();
            }
        });
    }

    private void deleteUser() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document(currentUser.getUid())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        getIntent().getExtras().clear();
                        setResult(1001, getIntent());
                        finish();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
